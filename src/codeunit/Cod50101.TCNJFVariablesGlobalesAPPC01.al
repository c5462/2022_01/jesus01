codeunit 50101 "TCNJFVariablesGlobalesAPPC01"
{
    SingleInstance = true;
    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    procedure GetSwitchF() xSalida: Boolean
    begin
        xSalida := xSwitch;
    end;
    /// <summary>
    /// Guarda el nombre a nivel global a BC
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        xName := pName;
    end;
    /// <summary>
    /// Devuelve el nombre guardado globalmente
    /// </summary>
    /// <returns>Return variable return of type Text.</returns>
    procedure NombreF() return: Text
    begin
        return := xName;
    end;

    var
        xSwitch: Boolean;
        xName: Text;
}