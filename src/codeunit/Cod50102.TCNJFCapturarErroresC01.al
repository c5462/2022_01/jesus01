codeunit 50102 "TCNJFCapturarErroresC01"
{
    trigger OnRun()
    var

        rlCustomer: Record Customer;
        cuCodLab: Label 'ABC', Locked = true, comment = 'El codifndgjbdifsgf ghdf vva ifhz dsfds fiszdh UWU';
    begin

        rlCustomer.get('');
        if not rlCustomer.Get(cuCodLab) then begin
            rlCustomer.Init();
            rlCustomer.Validate("No.", cuCodLab);
            rlCustomer.Insert(true);
            rlCustomer.Validate(Name, 'Proveedor prueba');
            rlCustomer.Validate(Address, 'Mi calle');
            rlCustomer."VAT Registration No." := 'xxxxx';
            rlCustomer.Validate("Payment Method Code");
            rlCustomer.Modify(true);
        end;

        Message('Datos del Cliente: %1', rlCustomer);

    end;

    procedure FuncionParaCpturarErroF(pCodProveedor: Code[20]): Boolean
    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.Get(pCodProveedor) then begin
            rlVendor.Init();
            rlVendor.Validate("No.", pCodProveedor);
            rlVendor.Insert(true);
            if AsignarDatosProveedor(rlVendor) then begin

                rlVendor.Modify(true);
            end else begin
                Message('Error en asignar datos (1%)', GetLastErrorText());
            end;

        end;
        Message('Ya existe el proveedor %1', pCodProveedor);
    end;

    [TryFunction]
    local procedure AsignarDatosProveedor(var rlVendor: Record Vendor)
    begin
        rlVendor.Validate(Name, 'Proveedor prueba');
        rlVendor.Validate(Address, 'Mi calle');
        rlVendor."VAT Registration No." := 'xxxxx';
        rlVendor.Validate("Payment Method Code", 'TCNJF');
    end;

}
