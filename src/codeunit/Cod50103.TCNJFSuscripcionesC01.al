codeunit 50103 "TCNJFSuscripcionesC01"
{
    SingleInstance = true;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]

    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'URL1', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventF(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        culTCNJFFuncionesAPPC01: Codeunit TCNJFFuncionesAPPC01;
    begin
        culTCNJFFuncionesAPPC01.MensajeFraCompraF(PurchInvHdrNo)
    end;

    [EventSubscriber(ObjectType::Table, Database::TCNJFLinPlanVacunacionC01, 'OnBeforeSigVacF', '', false, false)]
    local procedure DatabaseTCNJFLinPlanVacunacionC01OnBeforeSigVacFF(Rec: Record TCNJFLinPlanVacunacionC01; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeUnitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culTCNJFFuncionesAPPC01: Codeunit TCNJFFuncionesAPPC01;
    begin
        culTCNJFFuncionesAPPC01.CodeUnitSalesPostOnAfterPostSalesDocF(SalesHeader);
    end;

}