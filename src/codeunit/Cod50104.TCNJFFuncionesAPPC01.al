codeunit 50104 "TCNJFFuncionesAPPC01"
{
    procedure MensajeFraCompraF(PurchInvHdrNo: code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 Ha creado la factura %2', UserId, PurchInvHdrNo);
        end;
    end;

    local procedure FuncionEjemplo01F(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) return: Text
    var
        i: Integer;
    begin

    end;

    procedure EjemploVariableESF(var pTexto: Text)
    begin
        pTexto := 'A desayunar que son las 10'
    end;

    procedure EjemploVariableE02SF(pTexto: TextBuilder)
    begin
        pTexto.Append('A desayunar que son las 10')
    end;

    procedure EjemploArrayF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
        xlposJugada: Integer;
    begin
        Randomize();
        //rellena el array
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := Format(i);
        end;
        repeat
            xlposJugada := Random(CompressArray(mlNumeros));
            Message('Ha salido la bola nº %1', xlposJugada);
            mlNumeros[xlposJugada] := '';
        until CompressArray(mlNumeros) <= 0;
    end;

    procedure BingoF()

    var
        mlNumeros: Array[20] of Text;
        i: Integer;
        xlNumRandomObtenido: Integer;
    begin
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := Format(i);
        end;
        repeat
            xlNumRandomObtenido := Random(CompressArray(mlNumeros));
            Message('La bola que ha salido es %1', mlNumeros[xlNumRandomObtenido]);
            mlNumeros[xlNumRandomObtenido] := '';
        until CompressArray(mlNumeros) <= 0;

    end;

    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.")
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");

        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");

        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");

        if rlRec.FindLast() then begin

            exit(rlRec."Line No.");
        end;
    end;

    procedure EjempploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjempploSobrecargaF(p1, p2, p3, false);
    end;

    procedure EjempploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; pBooleano: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin

        p1 := 1;
        p2 := 12.45;
        p3 := 'lo que sea';
        if pBooleano then begin
            xlNuevoNumero := p1 * p2;

        end;
    end;

    procedure UsoFuncionEjemploF()
    begin
        EjempploSobrecargaF(10, 34.56, 'Hi sekai');
        EjempploSobrecargaF(110, 314.56, 'sayonara sekai');
    end;

    procedure SintaxisF()
    var
        rlCustomer: Record Customer;
        Ventana: Dialog;
        i: Integer;
        xlTexto: Text;
        x: Decimal;
        z: Integer;
        xBool: Boolean;
        xlPos: Integer;
        xlLong: Integer;
        xlCampos: List of [Text];
        clSeparador: Label '·', Locked = true;
        xlCampo: Text;
        xlFecha: Date;
    begin
        i := 1;
        i := i + 10;
        i += 10;
        xlTexto := 'Jesus';
        xlTexto := xlTexto + 'Fermin';
        xlTexto += 'Sanchez';
        //operador
        //redondea, indica a numero entero o que decimal, y se señala si quieras que sea a al mas pequeño o mas mayor
        i := Round(x + z, 1, '=');

        i := 9 div 2;
        //resto
        i := 9 mod 2;
        //devuelve valor absoluto
        i := Abs(-9);
        xBool := not true;
        xBool := false and true;
        xBool := false or true;
        xBool := false xor true;
        //operadores relacionales
        xBool := 1 < 2;
        xBool := 1 > 2;
        xBool := 1 <= 2;
        xBool := 1 >= 2;
        xBool := 1 = 2;
        xBool := 1 <> 2;
        xBool := 1 in [1, 'b', 3, 'y'];
        xBool := 'a' in ['a' .. 'z'];

        //funciones usadas
        Message('Esto es un mensaje y esto es un parametro que se le pasa %1', rlCustomer);
        Error('Esto es un mensaje que se produce por un error y asi se ve por quien ha sido el error %1', UserId);
        xBool := Confirm('Confirma que desea registrar la factura %1', false, 'FV2022-00001');
        i := StrMenu('Enviar, Facturar, Enviar y Facturar', 3, '¿Cómo desea registrar?');
        case i of
            0:
                Error('Proceso cancelado');
            1:
                ;
            2:
                ;
            3:
                ;
        end;
        if GuiAllowed then begin
            Ventana.Open('Procesando bucle i #1##########\' +
                         'Procesando bucle x #2##########');
        end;
        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i);
            end;
            for x := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(2, x);
                end;
            end;
        end;
        if GuiAllowed then begin
            Ventana.Close();
        end;
        //funciones de cadenas de texto

        xlLong := maxstrlen(xlTexto);
        xlLong := maxstrlen('ABC');
        xlTexto := CopyStr('ABC', 2, 1);//B
        xlTexto := CopyStr('ÀBC', 2);//BC
        xlTexto := xlTexto.Substring(2);
        xlPos := StrPos('Elefante', 'e');//3
        xlPos := xlTexto.IndexOf('e');
        xlLong := StrLen('ABCD    ');//4
        xlTexto := UpperCase(xlTexto);
        xlTexto := LowerCase(xlTexto);
        xlTexto := xlTexto.ToLower();
        xlTexto := xlTexto.ToUpper();
        rlCustomer.Validate("Search Name", rlCustomer.Name.ToUpper());
        xlTexto := xlTexto.TrimStart('\').TrimEnd('\').Trim() + '\';
        xlTexto := '123,456,Tecon Servicios,s.l.,789';
        Message(SelectStr(2, xlTexto));
        xlTexto := '123·456·Tecon Servicios·s.l.·789';
        message(xlTexto.Split('·').Get(2));

        xlTexto := '123·456·Tecon Servicios·s.l.·789';
        xlCampos := xlTexto.Split(clSeparador);
        Message(xlCampos.Get(4));
        ;
        Message(xlTexto.Split(clSeparador).Get(1));
        Message(xlTexto.Split(clSeparador).Get(2));
        Message(xlTexto.Split(clSeparador).Get(3));
        Message(xlTexto.Split(clSeparador).Get(4));
        foreach xlTexto in xlCampos do begin

        end;
        foreach xlCampo in xlTexto.Split(clSeparador) do begin

        end;
        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '/'));
        Evaluate(xlFecha, xlTexto.Replace('-', '/'));
        Evaluate(xlFecha, xlTexto.Replace('-', '/').Replace(',', '.'));

        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '>', 'e'));
        xlPos := Power(3, 2);
        xlTexto := UserId();
        xlTexto := CompanyName();
        today;
        time;
        WorkDate();

    end;

    procedure LeftF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        if pNumCaracteres > StrLen(pCadena) then begin
            Error('La cadena es superior a la lon de la cadena');
        end;
        exit(CopyStr(pCadena, 1, pNumCaracteres));
    end;

    procedure RightF(pCadena: Text; pNumCaracteres: Integer): Text
    begin

        exit(CopyStr(pCadena, (StrLen(pCadena) - pNumCaracteres), +1));
    end;

    procedure StreamsF()
    var
        TempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        xlInsStr: InStream;
        xOutStr: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLTCNJFConfiguracionC01.MiBlob.CreateOutStream(xOutStr, TextEncoding::UTF8);
        xOutStr.WriteText();
        TempLTCNJFConfiguracionC01.MiBlob.CreateInStream(xlInsStr);
        xlInsStr.ReadText(xlLinea);
        xlNombreFichero := 'Pedido CSV';
        if not DownloadFromStream(xlInsStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;
    end;

    procedure ClienteDiezMillF()
    rlCustomer: Record Customer;
    begin
        rlCustomer.Get('10000');
        //no se usa casi nunca, respeta filtros(SetFiler) y rangos(SetRange)
        rlCustomer.Find();
    end;

    procedure PedidoVentaSegurosF()
     rlSalesLine: Record "Sales Line";
    begin
        rlSalesLine.Get(rlSalesLine."Document Type"::Order, '101005', 10000);
    end;

    procedure PedidoDimensionF()
    rlDimensionSetEntry: Record "Dimension Set Entry";
    begin
        // rlDimensionSetEntry.Get(rlDimensionSetEntry."Dimension Set ID", rlDimensionSetEntry."Dimension Code"); 
        rlDimensionSetEntry.Get(7, 'GRUPONEGOCIO');
    end;

    procedure InformacionEmpresaF()
    rlCompanyInformation: Record "Company Information";
    begin
        rlCompanyInformation.Get();

    end;

    // procedure EliminarListaCabF()
    // var
    //     rlTCNJFCabPlanVacunacionC01: Record TCNJFCabPlanVacunacionC01;
    //     rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
    // begin

    // end;

    procedure OtrasFuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin

        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    procedure ToVendorF()
    var
        rlVendor: Record Vendor;
        rlCustomer: Record Customer;
    begin
        rlVendor.Init();
        rlVendor.TransferFields(rlCustomer, true, true);
        rlVendor.Insert(true);
        Message('Proveedor añadiddo');
    end;

    // local procedure UsoDeListas()
    // var
    //     xlLista: List of [Code[20]];
    // begin
    // end;

    procedure AbrirListaClientesF(pNotif: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodigoCLiente';
    begin
        if pNotif.HasData(clNombreDato) then begin
            rlCustomer.SetRange("No.", pNotif.GetData(clNombreDato));
        end;
        Page.Run(page::"Customer List");
    end;

    procedure CodeUnitSalesPostOnAfterPostSalesDocF(SalesHeader: Record "Sales Header")
    var
        rlSalesLine: Record "Sales Line";
        rlCustomer: Record Customer;
        xlNotificacion: Notification;
        clMensajeImporte: Label 'La linea %1 del documento %2 nº %3, con importe %4, es inferior a 100';
        clMsgVacuna: Label 'El cliente %1 no tiene vacuna';
    begin
        //2) verificar cliente tiene vacuna
        if rlCustomer.get(SalesHeader."Sell-to Customer No.") then begin
            if rlCustomer.TCNJFTipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clMsgVacuna, rlCustomer."No."));
                xlNotificacion.AddAction('Abrir Ficha Cliente', Codeunit::TCNJFFuncionesAPPC01, 'AbrirFichaClienteF');
                xlNotificacion.SetData('CodCte', rlCustomer."No.");
                xlNotificacion.Send();
            end;
        end;
        //verificar si sus lineas superan los 100 euros
        //si no mandar notificaciones para las lineas que no los superen
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlCustomer.SetFilter(Amount, '<%1', 100);
        if rlSalesLine.FindSet(false) then begin
            repeat
                Clear(xlNotificacion);
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clMensajeImporte, rlSalesLine."Line No.", SalesHeader."Document Type", rlSalesLine."Document No.", rlSalesLine.Amount));
                xlNotificacion.Send();

            until rlSalesLine.Next() = 0;
        end;
    end;

    procedure AbrirFichaClienteF(pNotif: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodCte';
    begin
        if pNotif.HasData(clNombreDato) then begin
            if rlCustomer.get(pNotif.GetData(clNombreDato)) then begin
                Page.Run(page::"Customer card", rlCustomer);
            end;
        end;
    end;

    procedure CumpleFiltroF(pCodiigo: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodiigo;
        TempLCustomer.Insert(false);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;
}