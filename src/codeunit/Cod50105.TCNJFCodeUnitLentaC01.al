codeunit 50105 "TCNJFCodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;

    local procedure EjecutarTareaF()

    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Desea ejecutar esta tarea que tarda mucho?')
        end;
        if xlEjecutar then begin
            InsertarLogF('Tarea iniciada');
            Commit();//usar bajo peligro de muerte
            Message('En proceso');
            Sleep(10000);
            InsertarLogF('Tarea finalizada')

        end;
    end;

    local procedure InsertarLogF(pMensaje: Text)

    rlTCNJFLogC01: Record TCNJFLogC01;
    begin
        rlTCNJFLogC01.Init();
        rlTCNJFLogC01.Insert(true);
        rlTCNJFLogC01.Validate(Mensaje, pMensaje);
        rlTCNJFLogC01.Modify(true);
    end;

}