table 50102 "TCNJFCabPlanVacunacionC01"
{
    Caption = 'Cab. Plan Vacunación';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; FechaInicioVacunacionPlanificada; Date)
        {
            Caption = 'Fecha Inicio Vacunacion Planificada';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            DataClassification = SystemMetadata;
            TableRelation = Vendor."No.";
        }
    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }




    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor;
    begin
        if rlVendor.Get(Rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
    begin
        rlTCNJFLinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
        rlTCNJFLinPlanVacunacionC01.DeleteAll(true);
    end;
}
