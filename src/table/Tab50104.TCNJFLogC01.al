table 50104 "TCNJFLogC01"
{
    Caption = 'LogC01';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; IdUnico; Guid)
        {
            Caption = 'IdUnico';
            DataClassification = SystemMetadata;

        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; IdUnico)
        {
            Clustered = true;
        }
        key(k2; SystemCreatedAt)
        {

        }

    }
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.IdUnico) then begin
            Rec.Validate(IdUnico, CreateGuid());
        end;
    end;
}
