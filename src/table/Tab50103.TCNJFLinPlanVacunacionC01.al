table 50103 "TCNJFLinPlanVacunacionC01"
{
    Caption = 'Lin Plan Vacunación C01';
    DataClassification = ToBeClassified;
    LookupPageId = TCNJFSubLinPlanVacunaC01;
    DrillDownPageId = TCNJFSubLinPlanVacunaC01;

    fields
    {

        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; NoLinea; Integer)
        {
            Caption = 'Nº Línea';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; CodClienteVacunar; Code[20])
        {
            Caption = 'Código Cliente a Vacunar';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Customer."No.";
        }
        field(4; FechaVacunacion; Date)
        {
            Caption = 'Fecha Vacunacion';
            DataClassification = OrganizationIdentifiableInformation;

        }
        field(5; CodigoVacuna; Code[20])
        {
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
            Caption = 'Tipo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(otros), Bloqueado = const(false));
        }
        field(7; Fecha2AVac; Date)
        {
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        key(PK; CodigoLineas, NoLinea)
        {
            Clustered = true;
        }
    }

    procedure NombreCLienteF(): Text
    var
        rtCustomer: Record Customer;
    begin
        if rtCustomer.Get(Rec.CodClienteVacunar) then begin
            exit(rtCustomer.Name);
        end;
    end;


    /*  procedure DescOtrosF(): Text
      var
          rlTCNJFVariosC01: Record TCNJFVariosC01;
      begin
          if rlTCNJFVariosC01.Get(rlTCNJFVariosC01.Tipo::otros, Rec.CodigoOtros) then begin
              exit(rlTCNJFVariosC01.Descripcion);
          end;
      end;
  
     procedure DescVacunaF(): Text
     begin
    if rlTCNJFVariosC01.Get(/*Enum::TCNJFTipoC01::Vacuna
    rlTCNJFVariosC01.Tipo::Vacuna, Rec.CodigoVacuna) then begin
        exit(rlTCNJFVariosC01.Descripcion);
    end;
    */
    //end;

    procedure DescripcionVariosF(pTipo: Enum TCNJFTipoC01; pCodigoVarios: Code[20]): Text
    var
        rlTCNJFVariosC01: Record TCNJFVariosC01;
    begin
        if rlTCNJFVariosC01.Get(pTipo, pCodigoVarios) then begin
            exit(rlTCNJFVariosC01.Descripcion);
        end;
    end;

    procedure SigVacF()
    var
        rlTCNJFVariosC01: Record TCNJFVariosC01;
        IsHandled: Boolean;
    begin
        OnBeforeSigVacF(Rec, xRec, rlTCNJFVariosC01, IsHandled, CurrFieldNo);
        if not IsHandled then begin
            if Rec.FechaVacunacion <> 0D then begin
                if rlTCNJFVariosC01.Get(rlTCNJFVariosC01.Tipo::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.Validate(Rec.Fecha2AVac, CalcDate(rlTCNJFVariosC01.Fecha2AVac, Rec.FechaVacunacion));
                end;
            end;
            OnAfterSigVacF(Rec, xRec, rlTCNJFVariosC01, CurrFieldNo);
        end;
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeSigVacF(Rec: Record TCNJFLinPlanVacunacionC01; xRec: Record TCNJFLinPlanVacunacionC01; rlTCNJFVariosC01: Record TCNJFVariosC01; var IsHandled: Boolean; pCurrFieldNo: Integer)
    begin


    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterSigVacF(Rec: Record TCNJFLinPlanVacunacionC01; xRec: Record TCNJFLinPlanVacunacionC01; rlTCNJFVariosC01: Record TCNJFVariosC01; CurrFieldNo: Integer)
    begin
    end;
}
