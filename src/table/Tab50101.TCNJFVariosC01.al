table 50101 "TCNJFVariosC01"
{
    Caption = 'Varios';
    DataClassification = ToBeClassified;
    Extensible = true;
    LookupPageId = TCNJFTipoListaC01;
    DrillDownPageId = TCNJFTipoListaC01;
    fields
    {
        field(1; Tipo; Enum TCNJFTipoC01)
        {
            Caption = 'Tipo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;

        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = SystemMetadata;
        }
        field(5; Fecha2AVac; DateFormula)
        {
            DataClassification = SystemMetadata;
            Caption = 'Fecha 2ª Vacunacion';

        }

    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }
    trigger OnDelete()
    begin
        Rec.TestField(Bloqueado, true);
        /*
        if not rec.Bloqueado then begin
            Error('No se permite elimiar el registro no bloqueado');
        end
*/
    end;
}
