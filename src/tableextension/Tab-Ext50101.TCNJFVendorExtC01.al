tableextension 50101 "TCNJFVendorExtC01" extends Vendor
{
    trigger OnAfterInsert()
    var

        culTCNJFVariablesGlobalesAPPC01: Codeunit TCNJFVariablesGlobalesAPPC01;
    begin
        rec.Validate(Name, culTCNJFVariablesGlobalesAPPC01.NombreF());

    end;
}
