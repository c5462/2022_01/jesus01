tableextension 50100 "TCNJFCustomerExtC01" extends Customer
{

    fields
    {
        field(50100; TCNJFNoVacunaC01; Integer)
        {
            //CodClienteVacunar  TCNJFSubLinPlanVacunaC01
            Caption = 'Nº de Vacunas';
            FieldClass = FlowField;
            CalcFormula = count(TCNJFLinPlanVacunacionC01 where(CodClienteVacunar = field("No.")));

        }

        field(50101; TCNJFFechaVacunaC01; Date)
        {
            Caption = 'Fecha ultima Vacuna';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = max(TCNJFLinPlanVacunacionC01.FechaVacunacion where(CodClienteVacunar = field("No.")));
        }

        field(50102; TCNJFTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = TCNJFVariosC01.Codigo where(Tipo = const(Vacuna));
            trigger OnValidate()
            var
                rlTCNJFVariosC01: Record TCNJFVariosC01;
            begin
                // rlTCNJFVariosC01.Get(rlTCNJFVariosC01.Tipo::Vacuna, Rec.TCNJFTipoVacunaC01)
                if rlTCNJFVariosC01.Get(enum::TCNJFTipoC01::Vacuna, Rec.TCNJFTipoVacunaC01) then begin
                    rlTCNJFVariosC01.TestField(Bloqueado, false);
                end;

            end;

        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culTCNJFVariablesGlobalesAPPC01: Codeunit TCNJFVariablesGlobalesAPPC01;
            begin
                culTCNJFVariablesGlobalesAPPC01.NombreF(Rec.Name)
            end;


        }

    }

}
