pageextension 50105 "TCNJFCompaniesC01" extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(TCNJFClientesC01; CLientesF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº clientes';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    CLientesF(true);
                end;
            }
            field(TCNJFproveedoresC01; ProveedoresF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Proveedores';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    ProveedoresF(true);
                end;
            }
            field(TCNJFCuentasC01; CuentasF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Cuentas';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    CuentasF(true);
                end;
            }
            field(TCNJFProductosC01; ProductosF(false))
            {
                ApplicationArea = all;
                Caption = 'Nº Productos';
                DrillDown = true;
                trigger OnDrillDown()
                var
                begin
                    ProductosF(true);
                end;
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(TCNJFCopiaSegC01)
            {
                Caption = 'Copia seguridad';
                ApplicationArea = All;
                Image = Export;
                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;
            }
            action(TCNJFRestaurarC01)
            {
                Caption = 'Restaurar';
                ApplicationArea = All;
                Image = Import;
                trigger OnAction()
                begin
                    TCNJFRestaurarDatos();
                end;
            }
        }
    }
    local procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());
                xlRecRef.Field(4).Validate('');
                xlRecRef.Modify(true);
            until xlRecRef.Next() = 0;
        end;
        //        xlRec.KeyIndex(1).FieldIndex()
    end;

    procedure TCNJFExportarDatos()
    var
        TempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlAllObjWithCaption: Record AllObjWithCaption;
        rlCompany: Record Company;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;
        i: Integer;
        xlTextBuilder: TextBuilder;
        xlOutStr: OutStream;
        xlinStr: inStream;
        xlFichero: Text;
    begin
        // 1) Bucle de empresas
        CurrPage.SetSelectionFilter(rlCompany);
        if rlCompany.FindSet(false) then begin
            repeat
                //inicio de empresa 
                //escribir en el fichero
                xlTextBuilder.Append('Inicio empresa<' + rlCompany.Name + ' > ' + culTypeHelper.CRLFSeparator());
                //bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::"G/L Account", Database::Customer, Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        //inicio tabla
                        xlTextBuilder.Append('Inicio Tabla<' + rlAllObjWithCaption."Object Name" + ' > ' + culTypeHelper.CRLFSeparator());
                        //bucle registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                xlTextBuilder.Append('Inicio registro<' + Format(xlRecRef.RecordId) + ' > ' + culTypeHelper.CRLFSeparator());
                                //bucle campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField()
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append(StrSubstNo('<%1>·<%2>', xlRecRef.FieldIndex(i), xlRecRef.FieldIndex(i).Value) + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;
                            until xlRecRef.Next() = 0;
                            xlTextBuilder.Append('fin registro<' + Format(xlRecRef.RecordId) + ' > ' + culTypeHelper.CRLFSeparator());
                        end;
                    until rlAllObjWithCaption.Next() = 0;

                    xlRecRef.Close();
                    //fin tabla
                    xlTextBuilder.Append('fin Tabla<' + rlAllObjWithCaption."Object Name" + ' > ' + culTypeHelper.CRLFSeparator());
                end;
                //fin empresa
                xlTextBuilder.Append('fin  empresa<' + rlCompany.Name + ' > ' + culTypeHelper.CRLFSeparator());
            until rlCompany.Next() = 0;
        end;
        TempLTCNJFConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        xlOutStr.WriteText(xlTextBuilder.ToText());
        TempLTCNJFConfiguracionC01.MiBlob.CreateInStream(xlinStr, TextEncoding::UTF8);
        xlFichero := 'copia.bck';
        DownloadFromStream(xlinStr, '', '', '', xlFichero);
    end;

    local procedure CopiaSeguridadF()
    var
        rlCompany: Record Company;
        rlAllObjWithCaption: Record AllObjWithCaption;
        TempConfC01: Record TCNJFConfiguracionC01 temporary;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;
        xlTextBuilder: TextBuilder;
        i: Integer;
        xlOutStr: OutStream;
        xlInStr: InStream;
        xlFichero: Text;
    begin
        CurrPage.SetSelectionFilter(rlCompany);
        //1) Bucle de empresas

        if rlCompany.FindSet(false) then begin
            repeat
                //Escribir en el fichero
                //Inicio empresa
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                //2) Bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '15|18|23|27', Database::"G/L Account", Database::Customer,
                Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        //Inicio tabla
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        //3) Bucle de registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat//Inicio registro
                                xlTextBuilder.Append('IR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                //4) Bucle campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append(StrSubstNo('<%1>#<%2>', xlRecRef.FieldIndex(i).Name, xlRecRef.FieldIndex(i).Value) + '>' + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;
                                xlTextBuilder.Append('FR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        xlRecRef.Close();
                        //Fin tabla
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;
            until rlCompany.Next() = 0;
        end;

        //Escribir en un contenedor
        TempConfC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        xlOutStr.WriteText(xlTextBuilder.ToText());
        TempConfC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        xlFichero := 'CopiaSeguridad.bck';
        if not DownloadFromStream(xlInStr, '', '', '', xlFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;

    // local procedure ValorCampoF(pFieldRef: FieldRef; pValor: text): Text
    // begin
    //     if pFieldRef.Type = pFieldRef.Type::Option then begin
    //     end else begin
    //         exit();
    //     end;
    // end;

    local procedure TCNJFRestaurarDatos()
    var
        TempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlAllObjWithCaption: Record AllObjWithCaption;
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlLinea: Text;
        xlRecRef: RecordRef;
    begin

        // 1)Subir fichero de copias de seguridad

        TempLTCNJFConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            // 2) recorrer el fichero
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                case true of
                    // 3)Si es inicio de tabla abrir el registro

                    xlLinea.StartsWith('IT<'):
                        xlRecRef.Open(NumTablaF(CopyStr(xlLinea, 4).TrimEnd('>')));

                    // 4)si es inicio de registro, inicializamos el registro
                    xlLinea.StartsWith('IR<'):

                        xlRecRef.Init();

                    // 5) por cada campo asignar el valor
                    xlLinea.StartsWith('<'):
                        if (xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal) /*and (xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal)*/ then begin
                            xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))).Value := TipoCampoF(xlRecRef.Field(NumCampoF(xlRecRef.Number, xlLinea.Split('#').Get(1).TrimStart('<').TrimEnd('>'))), xlLinea.Split('#').Get(2).TrimStart('<').TrimEnd('>'));
                        end;

                    // 6) si es fin de registro insertar el registro 
                    xlLinea.StartsWith('FR>'):
                        xlRecRef.Insert();
                    // 7) si es fin de la tabla, cerrar el registro
                    xlLinea.StartsWith('FT>'):

                        xlRecRef.Close();

                end;
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1)', GetLastErrorText());
        end;
    end;

    local procedure TipoCampoF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLCustomer: Record Customer temporary;
        TempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        TempLItem: Record Item temporary;
        xlBigInteger: BigInteger;
        xlBool: Boolean;
        xlDecimal: Decimal;
        xlGuid: Guid;
        i: Integer;
        xlDateTime: DateTime;
        xlDate: Date;
        xlTime: Time;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::BigInteger, pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBool, pValor) then begin
                    exit(xlBool);
                end;
            pFieldRef.Type::Blob:
                exit(TempLTCNJFConfiguracionC01.MiBlob);
            pFieldRef.Type::Decimal:
                if Evaluate(xlDecimal, pValor) then begin
                    exit(xlDecimal);
                end;
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTime, pValor) then begin
                    exit(xlDateTime);
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate);
                end;
            pFieldRef.Type::Media:
                exit(TempLCustomer.Image);
            pFieldRef.Type::MediaSet:
                exit(TempLItem.Picture);
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            else
                exit(pValor);
        end;
    end;

    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure NumTablaF(pNombreTabla: Text): Integer
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
    begin
        rlAllObjWithCaption.SetRange("Object Name", pNombreTabla);
        rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
        rlAllObjWithCaption.SetLoadFields("Object ID");
        if rlAllObjWithCaption.FindFirst() then begin
            exit(rlAllObjWithCaption."Object ID");
        end;

    end;

    local procedure CLientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        exit(DatosTablaF(rlCustomer, pDrillDown));
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillDown));
    end;


    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillDown));
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
    begin
        exit(DatosTablaF(rlItem, pDrillDown));
    end;



    local procedure DatosTablaF(pRecVarian: variant; var pDrillDown: Boolean): Integer
    var

        rlCompany: Record Company;
        xlRecRef: RecordRef;
    begin
        if rlCompany.Get(Rec.Name) then begin
            if pRecVarian.IsRecord then begin
                xlRecRef.GetTable(pRecVarian);
                if Rec.name <> CompanyName then begin
                    xlRecRef.ChangeCompany(Rec.name);
                end;
                if pDrillDown then begin
                    pRecVarian := xlRecRef;
                    Page.Run(0, pRecVarian);
                end else begin
                    exit(xlRecRef.Count);
                end;
            end;
        end;
    end;
}