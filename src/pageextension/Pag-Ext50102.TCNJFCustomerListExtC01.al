pageextension 50102 "TCNJFCustomerListExtC01" extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(TCNJFSwitchVariableC01)
            {
                ApplicationArea = ALL;
                Caption = 'Prueba variables globalesa a BC';
                trigger OnAction()
                var
                    cuTCNJFVariablesGlobalesAPPC01: Codeunit TCNJFVariablesGlobalesAPPC01;
                    clMensaje: Label 'El valor actual es %1. Lo cambiamos a %2';

                begin
                    //%1 >> cada vez que lo ponga es un valor mas que le puedo pasar. (Para mas %2, %3...)
                    Message(clMensaje, cuTCNJFVariablesGlobalesAPPC01.GetSwitchF(), not cuTCNJFVariablesGlobalesAPPC01.GetSwitchF());
                    cuTCNJFVariablesGlobalesAPPC01.SetSwitchF(not cuTCNJFVariablesGlobalesAPPC01.GetSwitchF());
                end;
            }
            action(TCNJFPruebaCuCapturarErroresC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba';
                trigger OnAction()
                var
                    cuTCNJFCapturarErroresC01: Codeunit TCNJFCapturarErroresC01;
                begin
                    if cuTCNJFCapturarErroresC01.Run() then begin
                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end else begin
                        Message('No se ha podido ejecutar la Cu. Motivo (%1)', GetLastErrorText());
                    end;
                end;
            }
            action(TCNJF2CuCapturarErroresC02C01)
            {
                ApplicationArea = all;
                Caption = 'Prueba errores';
                trigger OnAction()
                var
                    cuTCNJFCapturarErroresC01: Codeunit TCNJFCapturarErroresC01;
                    i: Integer;
                    xlCodProv: Code[20];
                begin

                    for i := 1 to 5 do begin

                        if cuTCNJFCapturarErroresC01.FuncionParaCpturarErroF(xlCodProv) then begin

                            Message('Se ha creado el proveedor %1 ', xlCodProv);

                        end else begin

                            Message('No se ha podido ejecutar la función Motivo: (%1)', GetLastErrorText());

                        end;
                        xlCodProv := IncStr(xlCodProv);
                    end;
                end;

            }
            action(TCNJFCambioProvC01)
            {
                ApplicationArea = all;
                Caption = 'Cambiar Cte. a Prov.';
                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                    clMensaje: Label 'Cliente añadido como proveedor';
                    xlConfirm: Boolean;

                    clError: Label 'Ya existe un proveedor con el codigo %1';
                begin
                    xlConfirm := true;
                    if GuiAllowed then begin
                        xlConfirm := Confirm('¿Esta seguro que desea convertir el cliente a proveedor?');
                    end;
                    if xlConfirm then begin
                        rlVendor.SetFilter("No.", Rec."No.");
                        if not rlVendor.FindSet() then begin
                            rlVendor.Init();
                            rlVendor.transferFields(Rec, true, true);
                            rlVendor.Insert(true);
                            Message(StrSubstNo(clMensaje, rlVendor."No."));
                        end else begin
                            Message(StrSubstNo(clError, rlVendor."No."));
                        end;
                    end else begin
                        Error('Proceso cancelado');
                    end;
                end;
            }
        }

        addlast(History)
        {
            action(TCNJFFrasABONOSC01)
            {
                Caption = 'Facturas y abonos';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    TCNJFFrasYAbonosF();
                end;
            }
            action(TCNJFExportarC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar';
                trigger OnAction()
                begin
                    ExportaCtesF();
                end;
            }
            action(TCNJFSumatorioC01)
            {
                Image = Calculate;
                ApplicationArea = all;
                Caption = 'Sumatorio 01';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0;

                    end;
                    Message('Total es %1, ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;

            }
            action(TCNJFSumatorioC02C01)
            {
                Image = Calculate;
                ApplicationArea = all;
                Caption = 'Sumatorio 02';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlDetailedCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('Total es %1, ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;

            }
            action(TCNJFSumatorioC03C01)
            {
                Image = Calculate;
                ApplicationArea = all;
                Caption = 'Sumatorio 03';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.CalcFields(Amount);
                    rlCustLedgEntry.SetLoadFields(Amount);
                    rlCustLedgEntry.SetAutoCalcFields(Amount);
                    if rlCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlCustLedgEntry.Amount;
                        until rlCustLedgEntry.Next() = 0;
                    end;
                    Message('Total es %1, ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;

            }
            action(TCNJFSumatorioC04C01)
            {
                Image = Calculate;
                ApplicationArea = all;
                Caption = 'Sumatorio 04';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlCustLedgEntry.Amount;
                    Message('Total es %1, ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;

            }
            action(TCNJFExportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar Excel';
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xlFila: Integer;
                begin
                    TempLExcelBuffer.CreateNewBook('Clientes');
                    //rellenar cabecera

                    TCNJFAsignarCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
                    TCNJFAsignarCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
                    TCNJFAsignarCeldaF(1, 3, Rec.FieldCaption(Address), true, true, TempLExcelBuffer);
                    TCNJFAsignarCeldaF(1, 4, Rec.FieldCaption("Balance (LCY)"), true, true, TempLExcelBuffer);
                    xlFila := 2;
                    //rellenar celdas
                    CurrPage.SetSelectionFilter(rlCustomer);
                    rlCustomer.SetAutoCalcFields("Balance (LCY)");
                    rlCustomer.SetLoadFields("No.", Name, Address, "Balance (LCY)");
                    if rlCustomer.FindSet(false) then begin
                        repeat
                            TCNJFAsignarCeldaF(xlFila, 1, rlCustomer."No.", false, false, TempLExcelBuffer);
                            TCNJFAsignarCeldaF(xlFila, 2, rlCustomer.Name, false, false, TempLExcelBuffer);
                            TCNJFAsignarCeldaF(xlFila, 3, rlCustomer.Address, false, false, TempLExcelBuffer);
                            TCNJFAsignarCeldaF(xlFila, 4, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                            xlFila += 1;
                        until rlCustomer.Next() = 0;
                    end;
                    TempLExcelBuffer.WriteSheet('', '', '');
                    TempLExcelBuffer.CloseBook();
                    TempLExcelBuffer.OpenExcel();
                end;
            }
            action(TCNJFImportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Excel';
                Image = Import;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xlFicheroCte: Text;
                    xlInStr: Instream;
                    xlHoja: Text;
                begin
                    if UploadIntoStream('Seleccione el Excel para importar clientes', '', 'Ficheros Excel (*.xls)|*.xls;*.xlsx;*.csv', xlFicheroCte, xlInStr) then begin
                        xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then begin
                            Error('Proceso cancelado');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInStr, xlHoja);
                        TempLExcelBuffer.ReadSheet();
                        TempLExcelBuffer.SetFilter("Row No.", '>%1', 1);
                        if TempLExcelBuffer.FindSet(false) then begin
                            repeat
                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        begin
                                            rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                        end;
                                    3:
                                        begin
                                            rlCustomer.Validate(Address, TempLExcelBuffer."Cell Value as Text");
                                        end;
                                    4:
                                        begin
                                            rlCustomer.Modify(true);
                                        end;
                                end
                            until TempLExcelBuffer.Next() = 0;
                        end;
                    end else begin
                        Error('No se pudo cargar el excel');
                    end;
                end;
            }
            action(TCNJFPaginaFiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Pagina de Filtros';
                Image = Filter;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlPaginaFiltros: FilterPageBuilder;
                    clClientes: Label 'Clientes';
                    clMov: Label 'Movimientos';
                    clPrecio: Label 'Precios';
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFiltros.AddField(clClientes, rlCustomer.TCNJFFechaVacunaC01);
                    xlPaginaFiltros.AddRecord(clMov, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clMov, rlCustLedgerEntry."Posting Date");
                    xlPaginaFiltros.AddRecord(clPrecio, rlSalesPrice);
                    xlPaginaFiltros.AddField(clPrecio, rlSalesPrice."Starting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        Message(xlPaginaFiltros.GetView(clClientes, true));
                        Message(xlPaginaFiltros.GetView(clMov, true));
                        Message(xlPaginaFiltros.GetView(clPrecio, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
        }
    }
    var
        rlSalesPrice: Record "Sales Price";

    /// <summary>
    /// AsignarCeldaF.
    /// </summary>
    /// <param name="pFila">Integer.</param>
    /// <param name="pColumna">Integer.</param>
    /// <param name="pContenidoCelda">Text.</param>
    /// <param name="pNegrita">Boolean.</param>
    /// <param name="pItalica">Boolean.</param>
    /// <param name="TempPExcelBuffer">Temporary VAR Record "Excel Buffer".</param>
    procedure TCNJFAsignarCeldaF(pFila: Integer; pColumna: Integer; pContenidoCelda: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true);
        TempPExcelBuffer.Validate("Cell Value as Text", pContenidoCelda);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalica);
        TempPExcelBuffer.Modify(true);
    end;

    /// <summary>
    /// TCNJFFrasYAbonosF.
    /// </summary>
    procedure TCNJFFrasYAbonosF()
    var
        TemplSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
    begin
        rlSalesInvoiceHeader.SetRange("Bill-to Customer No.", rec."No.");
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TemplSalesInvoiceHeader.Init();
                TemplSalesInvoiceHeader.Copy(rlSalesInvoiceHeader);
                TemplSalesInvoiceHeader.Insert(false)
            until rlSalesInvoiceHeader.Next() = 0;
        end;
        rlSalesCrMemoHeader.SetRange("Bill-to Customer No.", rec."No.");

        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TemplSalesInvoiceHeader.Init();
                TemplSalesInvoiceHeader.Copy(rlSalesCrMemoHeader);
                TemplSalesInvoiceHeader.Insert(false)
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        page.run(0, TemplSalesInvoiceHeader)
    end;

    local procedure ExportaCtesF()
    var
        TempLJJConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlCustomer: Record Customer;
        xlOutStream: OutStream;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4', Locked = true;
        clSeparador: Label '·', Locked = true;
        xlInStream: Instream;
        xlNombreFichero: Text;
    begin
        TempLJJConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::UTF8);
        // Escribir el registro
        rlCustomer := Rec;
        rlCustomer.Find();
        Rec.CalcFields("Balance (LCY)");
        xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparador, rlCustomer."No.", rlCustomer.Name, rlCustomer."Balance (LCY)"));
        xlOutStream.WriteText();
        TempLJJConfiguracionC01.MiBlob.CreateInStream(xlInStream);
        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('Problema al descargar fichero');
        end;
    end;


    // var
    //     cuTCNJFFuncionesAppC01: Codeunit TCNJFVariablesGlobalesAPPC01;
    //     xSwitch: Boolean;



}
/* Caption = 'Prueba';
 actions
 {
     addfirst(General)
     {
         action(TCNJFSwitchVariableC01)
         {
             ApplicationArea = all;
             Caption = 'Prueba variables globales a BC';
             trigger OnAction()
             var
                 clMensaje: Label 'El valor actual es %1. lo cambiamos a %2';
                 culTCNJFFuncionesAPPC01: Codeunit "TCNJFVariablesGlobalesAPPC01";
             begin
                 Message(clMensaje, culTCNJFFuncionesAPPC01.GetSwitchF(), not culTCNJFFuncionesAPPC01.GetSwitchF());
                 culTCNJFFuncionesAPPC01.SetSwitchF(not culTCNJFFuncionesAPPC01.GetSwitchF());
             end;
         }
         action(TCNJFPruebaCapturarErroresC01)
         {
             ApplicationArea = All;
             Caption = 'prueba codeunit de captura de errores';
             trigger OnAction()
             culTCNJFCapturarErroresC01: Codeunit TCNJFCapturarErroresC01;
             begin
                 if culTCNJFCapturarErroresC01.Run() then begin
                     Message('El boton dice que la funcion se ha ejecutado correctamente');

                 end else begin
                     Message('No se consiguio ejecutar la codeunit. Motivo (%1)', GetLastErrorText());
                 end;
             end;

         }
         action(TCNJFPruebaCapturarErroresC02C01)
         {
             ApplicationArea = All;
             Caption = 'prueba codeunit de captura de errores';
             trigger OnAction()
             culTCNJFCapturarErroresC01: Codeunit TCNJFCapturarErroresC01;
             begin
                 if culTCNJFCapturarErroresC01.FuncionCapturarErrorF() then begin
                     Message('El boton dice que la funcion se ha ejecutado correctamente');

                 end else begin
                     Message('No se consiguio ejecutar la funcion. Motivo (%1)', GetLastErrorText());
                 end;
             end;

         }
     }
 }
 var
}
*/
