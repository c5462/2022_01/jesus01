pageextension 50100 "TCNJFCustomerCardExtC01" extends "Customer Card"
{

    layout
    {
        addlast(content)
        {
            group(TCNJFVacunasC01)
            {
                Caption = 'Vacunas';

                field(TCNJFNoVacunaC01; Rec.TCNJFNoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Nº de Vacunas field.';
                    ApplicationArea = All;

                }
                field(TCNJFFechaVacunaC01; Rec.TCNJFFechaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha ultima Vacuna field.';
                    ApplicationArea = All;
                }
                field(TCNJFTipoVacunaC01; Rec.TCNJFTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de Vacuna field.';
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        addlast(processing)
        {
            action(TCNJFAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉💉💉Asigna Vacuna bloqueada';
                trigger OnAction()
                begin
                    Rec.Validate(TCNJFTipoVacunaC01, 'BLOQUEADO');
                end;
            }
            action(TCNJFVariableC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba variables globales a BC';
                trigger OnAction()
                var
                    clMensaje: Label 'El valor actual es %1. lo cambiamos a %2';

                begin
                    Message(clMensaje, cuTCNJFFuncionesAPPC01.GetSwitchF(), not cuTCNJFFuncionesAPPC01.GetSwitchF());
                    cuTCNJFFuncionesAPPC01.SetSwitchF(not cuTCNJFFuncionesAPPC01.GetSwitchF());
                end;
            }
        }
    }
    var
        cuTCNJFFuncionesAPPC01: Codeunit "TCNJFVariablesGlobalesAPPC01";

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta pagina?', false))
    end;

}

