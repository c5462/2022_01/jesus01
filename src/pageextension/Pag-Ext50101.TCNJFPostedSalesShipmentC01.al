pageextension 50101 "TCNJFPostedSalesShipmentC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(TCNJFEtiquetasC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                var
                    rlSalesShipmentLines: Record "Sales Shipment Line";
                    replEtiquetas: Report TCNJFEtiquetaC01;
                begin
                    rlSalesShipmentLines.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLines);
                    replEtiquetas.runModal();
                end;

            }
        }
    }
}