pageextension 50103 "TCNJFVendorListExitC01" extends "Vendor List"
{
    actions
    {
        addfirst(General)
        {
            action(TCNJFCambioProvC01)
            {
                ApplicationArea = all;
                Caption = 'Cambiar Prov. a Cte.';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    clMensaje: Label 'Proveedor añadido como cliente';
                    clError: Label 'Ya existe un cliente con el codigo %1';
                    xlConfirm: Boolean;
                begin
                    xlConfirm := true;
                    if GuiAllowed then begin
                        xlConfirm := Confirm('¿Esta seguro que desea convertir el cliente a proveedor?');
                    end;
                    if xlConfirm then begin
                        rlCustomer.SetFilter("No.", Rec."No.");
                        if not rlCustomer.FindSet() then begin
                            rlCustomer.Init();
                            rlCustomer.transferFields(Rec, true, true);
                            rlCustomer.Insert(true);
                            Message(StrSubstNo(clMensaje, rlCustomer."No."));
                        end else begin
                            Message(StrSubstNo(clError, rlCustomer."No."));
                        end;
                    end else begin
                        Error('Proceso cancelado');
                    end;
                end;
            }
        }
    }
}