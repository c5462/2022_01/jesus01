pageextension 50104 "TCNJFSalesOrderExpImpC01" extends "Sales Order List"
{
    //     actions
    //     {
    //         addfirst(Documents)
    //         {
    //             action(TCNJFExportC01)
    //             {
    //                 Caption = 'EXPORTAR';
    //                 ApplicationArea = All;
    //                 Image = Export;
    //                 trigger OnAction()
    //                 begin
    //                     ExportarPedidosVentasF();
    //                 end;
    //             }
    //             action(TCNJFImportC01)
    //             {
    //                 ApplicationArea = All;
    //                 Image = Import;
    //                 Caption = 'IMPORTAR';
    //                 trigger OnAction()
    //                 begin
    //                     ImportarPedidosVentaF();
    //                 end;
    //             }
    //         }
    //     }
    //     local procedure ExportarPedidosVentasF()
    //     var
    //         rlSalesHeaderC01: Record "Sales Header"; //Cabecera de pedidos venta
    //      //   pgSalesOrderC01: Page TCNJFSalesHeaderC02; //Pagina Sales Header
    //         rlSalesLineC01: Record "Sales Line";
    //         TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
    //         xlInStr: InStream;
    //         xlOutStr: OutStream;
    //         xlLinea: Text;
    //         xlNombreFichero: Text;
    //         clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
    //         clCadenaLinea: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
    //         clSeparador: Label '·', Locked = true;
    //     begin
    //         TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
    //         Message('Seleccione Pedidos de venta a exportar');
    //         pgSalesOrderC01.LookupMode(true);
    //         pgSalesOrderC01.SetRecord(rlSalesHeaderC01);
    //         if pgSalesOrderC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
    //             pgSalesOrderC01.GetSelectionFilterF(rlSalesHeaderC01);
    //             //Obtenemos el dataset
    //             if rlSalesHeaderC01.FindSet(false) then begin
    //                 repeat
    //                     xlOutStr.WriteText('C'); //Para identificar que es la cabecera
    //                     xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
    //                     rlSalesHeaderC01."Document Type", clSeparador,
    //                     rlSalesHeaderC01."No.", clSeparador,
    //                     rlSalesHeaderC01.Amount,
    //                     rlSalesHeaderC01."Bill-to Name", clSeparador,
    //                     rlSalesHeaderC01."Bill-to Customer No."));
    //                     xlOutStr.WriteText();
    //                     //Recorremos las líneas de vacuna para exportarlas
    //                     rlSalesLineC01.SetRange("Document No.", rlSalesHeaderC01."No.");
    //                     if rlSalesLineC01.FindSet(false) then begin
    //                         repeat
    //                             xlOutStr.WriteText('L'); //Para identificar que es la línea
    //                             xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
    //                             rlSalesHeaderC01."Document Type", clSeparador,
    //                             rlSalesHeaderC01."No.", clSeparador,
    //                             rlSalesHeaderC01.Amount,
    //                             rlSalesHeaderC01."Bill-to Name", clSeparador,
    //                             rlSalesHeaderC01."Bill-to Customer No."));
    //                             xlOutStr.WriteText();
    //                         until rlSalesLineC01.Next() = 0;
    //                     end;
    //                 until rlSalesHeaderC01.Next() = 0;
    //             end;
    //         end else begin
    //             Error('Proceso cancelado por el usuario');
    //         end;
    //         TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
    //         xlNombreFichero := 'PedidosVenta.csv';
    //         if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
    //             Error('No se ha podido descargar el fichero');
    //         end;
    //     end;

    //     local procedure ImportarPedidosVentaF() //Mérito de Niko
    //     var
    //         rlSalesHeader: Record "Sales Header";
    //         xlInStr: Instream;
    //         xlLinea: Text;
    //         rlSalesLine: Record "Sales Line";
    //         TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
    //         clSeparador: Label '·', Locked = true;
    //         xlFecha: Date;
    //         xlLNum: Integer;
    //         xlTipo: Enum "Sales Document Type";
    //         xlAmount: Decimal;
    //         xltipoL: Enum "Sales Line Type";
    //         xlStatus: Enum "Sales Document Status";
    //         xlEnum: Enum "Acc. Schedule Line Show";

    //     begin
    //         TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
    //         if UploadIntoStream('', xlInStr) then begin
    //             while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
    //                 xlInStr.ReadText(xlLinea);
    //                 //Insertar Cabecera o líneas en funcion del primer caracter de la línea
    //                 case true of
    //                     xlLinea[1] = 'C':
    //                         //DAR DE ALTA CABECERA
    //                         begin
    //                             rlSalesHeader.Init();
    //                             if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
    //                                 rlSalesHeader.Validate("Document Type", xlEnum);
    //                             end;
    //                             rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
    //                             rlSalesHeader.Insert(true);
    //                             if Evaluate(xlAmount, xlLinea.Split(clSeparador).Get(5)) then begin
    //                                 rlSalesHeader.Validate(Amount, xlAmount);
    //                             end;
    //                             rlSalesHeader.Validate("Bill-to Name", xlLinea.Split(clSeparador).Get(4));
    //                             rlSalesHeader.Validate("Bill-to Customer No.", xlLinea.Split(clSeparador).Get(4));
    //                             rlSalesHeader.Modify(true);
    //                         end;
    //                     xlLinea[1] = 'L':
    //                         begin
    //                             rlSalesHeader.Init();
    //                             if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
    //                                 rlSalesHeader.Validate("Document Type", xlEnum);
    //                             end;
    //                             rlSalesHeader.Validate("No.", xlLinea.Split(clSeparador).Get(2));
    //                             rlSalesHeader.Insert(true);
    //                             if Evaluate(xlAmount, xlLinea.Split(clSeparador).Get(5)) then begin
    //                                 rlSalesHeader.Validate(Amount, xlAmount);
    //                             end;
    //                             rlSalesHeader.Validate("Bill-to Name", xlLinea.Split(clSeparador).Get(4));
    //                             rlSalesHeader.Validate("Bill-to Customer No.", xlLinea.Split(clSeparador).Get(4));
    //                             rlSalesHeader.Modify(true);
    //                         end;
    //                     else
    //                         Error('Tipo de línea %1 no reconocido', xlLinea[1]);
    //                 end
    //             end;
    //         end else begin
    //             Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
    //         end;
    //     end;
}

