page 50106 "TCNJFSubLinPlanVacunaC01"
{
    Caption = 'Sub Línea Plan Vacuna C01';
    PageType = ListPart;
    SourceTable = TCNJFLinPlanVacunacionC01;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoLineas)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                    Visible = false;
                    Editable = false;
                }
                field(NoLinea; Rec.NoLinea)
                {
                    ToolTip = 'Specifies the value of the Nº Línea field.';
                    ApplicationArea = All;

                    Visible = false;
                    Editable = false;
                }
                field(CodClienteVacunar; Rec.CodClienteVacunar)
                {
                    ToolTip = 'Specifies the value of the Código Cliente a Vacunar field.';
                    ApplicationArea = All;


                    trigger OnValidate()
                    var
                        rlTCNJFCabPlanVacunacionC01: Record TCNJFCabPlanVacunacionC01;
                    begin
                        if Rec.FechaVacunacion = 0D then begin
                            if rlTCNJFCabPlanVacunacionC01.Get(rec.CodigoLineas) then begin
                                rec.Validate(FechaVacunacion, rlTCNJFCabPlanVacunacionC01.FechaInicioVacunacionPlanificada);
                            end;
                        end;
                    end;
                }
                field(NombreCliente; Rec.NombreCLienteF())
                {
                    Caption = 'Nombre Cliente';
                    ApplicationArea = all;
                }

                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ApplicationArea = all;
                    ToolTip = '';

                    trigger OnValidate()
                    var
                        rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    begin
                        rlTCNJFLinPlanVacunacionC01.SigVacF();
                    end;
                }
                field(Fecha2AVac; Rec.Fecha2AVac)
                {

                    ApplicationArea = all;
                    ToolTip = '';
                }
                field(CodigoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                }
                field(DescOtros; Rec.DescripcionVariosF(eTCNJFTipoC01::Vacuna, Rec.CodigoVacuna))
                {
                    Caption = 'Otros';
                    ApplicationArea = all;
                }
                field(CodigoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;

                    trigger OnValidate()
                    var
                        rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    begin
                        rlTCNJFLinPlanVacunacionC01.SigVacF();
                    end;
                }
                field(DescVacuna; Rec.DescripcionVariosF(eTCNJFTipoC01::Otros, Rec.CodigoOtros))
                {
                    Caption = 'Descripcion';
                    ApplicationArea = all;
                }
            }
        }
    }

    var
        eTCNJFTipoC01: Enum TCNJFTipoC01;



    /*
        trigger OnNewRecord(BelowxRec: Boolean)
        var
            rlTCNJFCabPlanVacunacionC01: Record TCNJFCabPlanVacunacionC01;
        begin
            if rlTCNJFCabPlanVacunacionC01.Get(Rec.CodigoLineas) then begin
                Rec.Validate(FechaVacunacion, rlTCNJFCabPlanVacunacionC01.FechaInicioVacunacionPlanificada);
            end;
        end;*/
}
