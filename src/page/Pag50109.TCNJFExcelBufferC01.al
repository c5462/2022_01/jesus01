page 50109 "TCNJFExcelBufferC01"
{
    Caption = 'ExcelBufferC01';
    PageType = List;
    SourceTable = "Excel Buffer";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Bold; Rec.Bold)
                {
                    ToolTip = 'Specifies the value of the Bold field.';
                    ApplicationArea = All;
                }
                field("Cell Type"; Rec."Cell Type")
                {
                    ToolTip = 'Specifies the value of the Cell Type field.';
                    ApplicationArea = All;
                }
                field("Cell Value as Blob"; Rec."Cell Value as Blob")
                {
                    ToolTip = 'Specifies the value of the Cell Value as Blob field.';
                    ApplicationArea = All;
                }
                field("Cell Value as Text"; Rec."Cell Value as Text")
                {
                    ToolTip = 'Specifies the value of the Cell Value as Text field.';
                    ApplicationArea = All;
                }
                field("Column No."; Rec."Column No.")
                {
                    ToolTip = 'Specifies the value of the Column No. field.';
                    ApplicationArea = All;
                }
                field(Comment; Rec.Comment)
                {
                    ToolTip = 'Specifies the value of the Comment field.';
                    ApplicationArea = All;
                }
                field("Double Underline"; Rec."Double Underline")
                {
                    ToolTip = 'Specifies the value of the Double Underline field.';
                    ApplicationArea = All;
                }
                field(Formula; Rec.Formula)
                {
                    ToolTip = 'Specifies the value of the Formula field.';
                    ApplicationArea = All;
                }
                field(Formula2; Rec.Formula2)
                {
                    ToolTip = 'Specifies the value of the Formula2 field.';
                    ApplicationArea = All;
                }
                field(Formula3; Rec.Formula3)
                {
                    ToolTip = 'Specifies the value of the Formula3 field.';
                    ApplicationArea = All;
                }
                field(Formula4; Rec.Formula4)
                {
                    ToolTip = 'Specifies the value of the Formula4 field.';
                    ApplicationArea = All;
                }
                field(Italic; Rec.Italic)
                {
                    ToolTip = 'Specifies the value of the Italic field.';
                    ApplicationArea = All;
                }
                field(NumberFormat; Rec.NumberFormat)
                {
                    ToolTip = 'Specifies the value of the NumberFormat field.';
                    ApplicationArea = All;
                }
                field("Row No."; Rec."Row No.")
                {
                    ToolTip = 'Specifies the value of the Row No. field.';
                    ApplicationArea = All;
                }
                field(xlColID; Rec.xlColID)
                {
                    ToolTip = 'Specifies the value of the xlColID field.';
                    ApplicationArea = All;
                }
                field(xlRowID; Rec.xlRowID)
                {
                    ToolTip = 'Specifies the value of the xlRowID field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
