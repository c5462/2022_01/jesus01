page 50100 "TCNJFConfiguracionC01"
{
    Caption = 'Configuracion de la app 01 del curso';
    PageType = Card;
    SourceTable = TCNJFConfiguracionC01;
    AboutTitle = 'En esta página crearemos la configuracion de la primera app del curso de especialista de business central 2022';
    AboutText = 'Fugiat excepteur sunt et culpa consequat. Veniam velit irure aute do ullamco duis aliqua reprehenderit elit cillum pariatur ex. Nulla amet Lorem proident ullamco.';
    AdditionalSearchTerms = 'App de Curso 01';
    ApplicationArea = all;
    UsageCategory = Administration;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter = true;
    SaveValues = true;
    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodClienteWeb; Rec.CodClienteWeb)
                {
                    ToolTip = 'Specifies the value of the Cod cliente para web field.';
                    ApplicationArea = All;
                    AboutTitle = 'En pagina crearemos la configuracion de la primera app del curs de especialistas de business central 2022';
                    AboutText = 'Velit dolore eu reprehenderit amet proident reprehenderit adipisicing ex aute quis. Voluptate fugiat eu nostrud fugiat magna irure. Aute proident anim laboris ad nisi commodo irure enim ullamco. Nostrud laboris veniam excepteur est deserunt exercitation anim dolore anim irure nisi exercitation sunt. Ea minim voluptate nostrud fugiat reprehenderit culpa sint est voluptate cupidatat ea officia dolor. Exercitation et magna ea consequat ex ad esse ut adipisicing ipsum qui dolore veniam eu. Est qui sunt et nostrud.';
                    Importance = Promoted;

                }

                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Hola github';
                    AboutText = 'Github controlame esto por diosito';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;

                    AboutTitle = 'Hola github otra vez';
                    AboutText = 'Github controlame esto por diosito2';
                }
                field(Password; xPassword)
                {
                    ApplicationArea = All;
                }
                field(NumInteracciones; xNum)
                {
                    Caption = 'Nº de interacciones';
                    ApplicationArea = All;
                }
            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CampoCalculado)
            {
                Caption = 'Campos calculados';
                field(NumUnidadesVendidas; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Muestra nombre del cliente';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(NombreClienteFuncion; Rec.NombreClienteF(Rec.CodClienteWeb))
                {
                    Caption = 'Nombre cliente';
                    ApplicationArea = All;
                }
            }
            group(TablaPrueba)
            {
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the TipoTabla field.';
                    ApplicationArea = All;
                }

                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; Rec.NombreTablaF(Rec.TipoTabla, Rec.CodTabla))
                {
                    Caption = 'Nombre de la tabla';
                    ApplicationArea = All;
                    Editable = false;
                }

                field(ColorFondo; Rec.ColorFondo)
                {
                    Caption = 'Color fondo';
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    Caption = 'Color letra';
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = all;
                Caption = 'Mensaje de accion';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Accion pulsada');
                end;
            }
            action(PedirDatos)
            {
                ApplicationArea = all;
                Caption = 'Pedir datos varios';
                trigger OnAction()
                var
                    pglTCNJFEntradaDatosC01l: Page TCNJFEntradaDatosC01;
                    xlBool: Boolean;
                    xlDecimal: Decimal;
                    xlDate: Date;
                begin
                    pglTCNJFEntradaDatosC01l.CampoF('Ciudad de nacimiento', 'Albacete');
                    pglTCNJFEntradaDatosC01l.CampoF('Pais de nacimiento', 'Españita');
                    pglTCNJFEntradaDatosC01l.CampoF('¿Cuando es tu cumpleaños?', 'hoy');
                    pglTCNJFEntradaDatosC01l.CampoF('¿Cuota pagada?', false);
                    pglTCNJFEntradaDatosC01l.CampoF('¿coche pagado?', true);
                    pglTCNJFEntradaDatosC01l.CampoF('¿Cuanto dinero tiene?', 10);
                    pglTCNJFEntradaDatosC01l.CampoF('¿2x2?', 4);
                    pglTCNJFEntradaDatosC01l.CampoF('¿raiz cuadrada de 9?', 3);
                    pglTCNJFEntradaDatosC01l.CampoF('ahora es?', Today);
                    pglTCNJFEntradaDatosC01l.CampoF('Ayer fue', Today - 1);
                    pglTCNJFEntradaDatosC01l.CampoF('Texto', '4');

                    if pglTCNJFEntradaDatosC01l.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
                        Message(pglTCNJFEntradaDatosC01l.CampoF());
                        Message(pglTCNJFEntradaDatosC01l.CampoF());
                        Message(pglTCNJFEntradaDatosC01l.CampoF());
                        Message('Acion aceptada');
                        Message('Cuota %1', pglTCNJFEntradaDatosC01l.CampoF(xlBool));
                        pglTCNJFEntradaDatosC01l.CampoF(xlBool);
                        Message('Coche %1', xlBool);
                        pglTCNJFEntradaDatosC01l.CampoF(xlBool);
                        Message('Casa %1', xlBool);
                        Message('dinero %1', pglTCNJFEntradaDatosC01l.CampoF(xlDecimal));
                        pglTCNJFEntradaDatosC01l.CampoF(xlDecimal);
                        Message('multiplicacion %1', xlDecimal);
                        Message('hoy %1', pglTCNJFEntradaDatosC01l.CampoF(xlDate));
                        pglTCNJFEntradaDatosC01l.CampoF(xlDate);
                        Message('ayer %1', xlDate);
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(CambiarPassw0rd)
            {
                ApplicationArea = all;
                Caption = 'Cambiar contraseña';
                trigger OnAction()
                begin

                    CambiarPasswordF();
                end;

            }
            action(ExportarVacunas)
            {
                ApplicationArea = all;
                Caption = 'Exportar Vacunas';
                trigger OnAction()
                var
                begin
                    ExportarVacunasDF();
                end;

            }
            action(ExportarPedidosVentas)
            {
                ApplicationArea = all;
                Caption = 'Exportar Pedidos venta';
                trigger OnAction()
                var
                begin
                    ExportarPedidosVentasF();
                end;

            }

            action(PruebaTxt)
            {
                Caption = 'Pruebas con Text';
                ApplicationArea = All;

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin

                    xlInicio := CurrentDateTime;
                    for i := 1 to xNum do begin
                        xlTxt += '.';
                    end;
                    Message('Tiempo invertido (%1)', CurrentDateTime - xlInicio);
                end;
            }
            action(PruebaTextBuilder)
            {
                Caption = 'Pruebas con Text Buielder';
                ApplicationArea = All;

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: TextBuilder;
                begin

                    xlInicio := CurrentDateTime;
                    for i := 1 to xNum do begin
                        xlTxt.Append('.');
                    end;
                    Message('Tiempo invertido (%1)', CurrentDateTime - xlInicio);
                end;
            }
            action(ContarClientes)
            {
                Caption = 'Contar Clientes';
                ApplicationArea = All;
                // customer ledger entry
                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlFilterPageBuilder: FilterPageBuilder;
                    xlLista: List of [Code[20]];
                    clCustomerEntry: Label 'Mov Clientes';
                begin
                    xlFilterPageBuilder.PageCaption('Seleccione los registros a contar');
                    xlFilterPageBuilder.AddRecord(clCustomerEntry, rlCustLedgerEntry);
                    xlFilterPageBuilder.AddField(clCustomerEntry, rlCustLedgerEntry."Customer No.");
                    if xlFilterPageBuilder.RunModal() then begin
                        rlCustLedgerEntry.SetView(xlFilterPageBuilder.GetView(clCustomerEntry));
                        rlCustLedgerEntry.SetLoadFields("Customer No.");
                        if rlCustLedgerEntry.FindSet(false) then begin
                            repeat
                                if not xlLista.Contains(rlCustLedgerEntry."Customer No.") then begin
                                    xlLista.Add(rlCustLedgerEntry."Customer No.");
                                end;
                            until rlCustLedgerEntry.Next() = 0;
                        end else begin
                            Message('No existen registros dentro del filtro %1', rlCustLedgerEntry.GetFilters);
                        end;
                        //  Message(xlPaginaFiltros.GetView(clCustomerEntry, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                    Message('El numero de clientes es %1', xlLista.Count);
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;

    [NonDebuggable]
    local procedure CambiarPasswordF()
    var
        pglTCNJFEntradaDatosC01l: Page TCNJFEntradaDatosC01;
        xlPass: Text;
    begin
        pglTCNJFEntradaDatosC01l.CampoF('Contraseña actual', '', true);
        pglTCNJFEntradaDatosC01l.CampoF('Nueva Contraseña', '', true);
        pglTCNJFEntradaDatosC01l.CampoF('confirmar Contraseña', '', true);
        if pglTCNJFEntradaDatosC01l.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            if pglTCNJFEntradaDatosC01l.CampoF(xlPass) = xPassword then begin
                if pglTCNJFEntradaDatosC01l.CampoF(xlPass) = pglTCNJFEntradaDatosC01l.CampoF(xlPass) then begin
                    xPassword := xlPass;
                    Message('Pass cambiada');
                end else begin
                    Error('Nuevo pass no coincide');
                end;
                Error('pass actual no coincide');
            end;
        end else begin
            Error('Proceso cancelado por usuario');
        end;
    end;


    procedure ExportarVacunasDF()

    var
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
        rlCabPlanVacunacionC01: Record TCNJFCabPlanVacunacionC01;
        pgListaCabPlanVacunacionC01: Page TCNJFListaCabPlanVacunacionC01;

        xlInStr: InStream;
        xlOutStr: OutStream;
        xlNombreFichero: Text;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clCadenaLinea: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
        clSeparador: Label '·', Locked = true;

    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr);
        Message('Seleccione vacunas a exportar');
        pgListaCabPlanVacunacionC01.LookupMode(true);
        pgListaCabPlanVacunacionC01.SetRecord(rlCabPlanVacunacionC01);
        if pgListaCabPlanVacunacionC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            pgListaCabPlanVacunacionC01.GetSelectionFilterF(rlCabPlanVacunacionC01);
            //Obtenemos el dataset
            if rlCabPlanVacunacionC01.FindSet(false) then begin
                repeat
                    xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador, rlCabPlanVacunacionC01.CodigoCabecera, rlCabPlanVacunacionC01.Descripcion, rlCabPlanVacunacionC01.EmpresaVacunadora, rlCabPlanVacunacionC01.FechaInicioVacunacionPlanificada));
                    xlOutStr.WriteText();
                    rlTCNJFLinPlanVacunacionC01.SetRange(CodigoLineas, rlCabPlanVacunacionC01.CodigoCabecera);
                    if rlTCNJFLinPlanVacunacionC01.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText(StrSubstNo(clCadenaLinea, clSeparador, rlTCNJFLinPlanVacunacionC01.CodigoLineas, rlTCNJFLinPlanVacunacionC01.CodClienteVacunar, rlTCNJFLinPlanVacunacionC01.CodigoOtros, rlTCNJFLinPlanVacunacionC01.CodigoVacuna, rlTCNJFLinPlanVacunacionC01.FechaVacunacion, rlTCNJFLinPlanVacunacionC01.Fecha2AVac));
                            xlOutStr.WriteText();
                        until rlTCNJFLinPlanVacunacionC01.Next() = 0;
                    end;
                until rlCabPlanVacunacionC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'Vacunas.json';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;


    procedure AccionAceptadaF(pAccion: Action): Boolean
    begin
        exit(pAccion in [pAccion::LookupOK, pAccion::OK, pAccion::Yes]);
    end;


    var
        xNum: Integer;
        cSeparador: Label ' ';
        xPassword: Text;

    procedure ExportarPedidosVentaF()
    var

        tempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlSalesLine: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlOutStr: OutStream;
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        xlTextBuilder: TextBuilder;
        culTypeHelper: Codeunit "Type Helper";
    begin
        CurrPage.SetSelectionFilter(rlSalesHeader);
        if rlSalesHeader.FindSet(false) then begin
            tempLTCNJFConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            repeat
                xlTextBuilder.Append('C' + cSeparador);
                xlTextBuilder.Append(Format(rlSalesHeader."Document Type") + cSeparador);
                xlTextBuilder.Append(rlSalesHeader."No." + cSeparador);
                xlTextBuilder.Append(rlSalesHeader."Sell-to Customer No." + cSeparador);
                xlTextBuilder.Append(rlSalesHeader."Sell-to Customer Name" + cSeparador);
                xlTextBuilder.Append(Format(rlSalesHeader."Posting Date", 0, 9));
                xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                rlSalesLine.SetRange("Document Type", rlSalesHeader."Document Type");
                rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
                if rlSalesLine.FindSet(false) then begin
                    repeat
                        xlTextBuilder.Append('L' + cSeparador);
                        xlTextBuilder.Append(Format(rlSalesLine."Line No.", 0, 9) + cSeparador);
                        xlTextBuilder.Append(Format(rlSalesLine.Type, 0, 9) + cSeparador);
                        xlTextBuilder.Append(rlSalesLine."No." + cSeparador);
                        xlTextBuilder.Append(rlSalesLine.Description + cSeparador);
                        xlTextBuilder.Append(Format(rlSalesLine.Quantity, 0, 9) + cSeparador);
                        xlTextBuilder.Append(Format(rlSalesLine."Unit Price", 0, 9) + cSeparador);
                        xlTextBuilder.Append(culTypeHelper.CRLFSeparator());
                    until rlSalesLine.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;
            tempLTCNJFConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            xlOutStr.WriteText(xlTextBuilder.ToText());
            tempLTCNJFConfiguracionC01.MiBlob.CreateInStream(xlInStr);
            xlNombreFichero := 'Pedidos.csv';
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                Error('no pudo descargar el fichero');
            end;
        end;
    end;

    local procedure ExportarPedidosVentasF()
    var
        rlSalesLineC01: Record "Sales Line";
        TempLConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
        rlSalesHeaderC01: Record "Sales Header"; //Cabecera de pedidos venta
        pgSalesOrderC01: Page TCNJFSalesHeaderC01; //Pagina Sales Header
        xlInStr: InStream;
        xlOutStr: OutStream;
        xlNombreFichero: Text;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clCadenaLinea: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
        clSeparador: Label '·', Locked = true;
    begin
        TempLConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        Message('Seleccione Pedidos de venta a exportar');
        pgSalesOrderC01.LookupMode(true);
        pgSalesOrderC01.SetRecord(rlSalesHeaderC01);
        if pgSalesOrderC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            pgSalesOrderC01.GetSelectionFilterF(rlSalesHeaderC01);
            //Obtenemos el dataset
            if rlSalesHeaderC01.FindSet(false) then begin
                repeat
                    xlOutStr.WriteText('C'); //Para identificar que es la cabecera
                    xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, clSeparador,
                    rlSalesHeaderC01."No.", clSeparador,
                    rlSalesHeaderC01."Document Type", clSeparador,
                    rlSalesHeaderC01."Sell-to Customer No.", clSeparador,
                    rlSalesHeaderC01.Amount));
                    xlOutStr.WriteText();
                    //Recorremos las líneas de vacuna para exportarlas
                    rlSalesLineC01.SetRange("Document No.", rlSalesHeaderC01."No.");
                    if rlSalesLineC01.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText('L'); //Para identificar que es la línea
                            xlOutStr.WriteText(StrSubstNo(clCadenaLinea, clSeparador,
                            rlSalesLineC01."Line No.", clSeparador,
                            rlSalesLineC01."Document No.", clSeparador,
                            rlSalesLineC01."Document Type", clSeparador,
                            rlSalesLineC01."Sell-to Customer No."));
                            xlOutStr.WriteText();
                        until rlSalesLineC01.Next() = 0;
                    end;
                until rlSalesHeaderC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        TempLConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'PedidosVenta.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('No se ha podido descargar el fichero');
        end;
    end;








    // local procedure ImportarPedidosF()
    // var
    //     TempLTCNJFConfiguracionC01: Record TCNJFConfiguracionC01 temporary;
    //     TempLTCNJFLogC01: Record TCNJFLogC01 temporary;
    //     rlSalesHeader: Record "Sales Header";
    //     rlSalesLine: Record "Sales Line";
    //     clError: Label 'Asignando CabVenta %1. error %2';
    //     xlInStr: InStream;
    //     xlLinea: Text;
    //     i: Integer;
    //     culTCNJFVariablesGlobalesAPPC01: Codeunit TCNJFVariablesGlobalesAPPC01;
    // begin
    //     TempLTCNJFConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
    //     if UploadIntoStream('', xlInStr) then begin
    //         while not xlInStr.EOS do begin
    //             xlInStr.ReadText(xlLinea);
    //             case xlLinea.ToUpper() [1] of
    //                 'C':
    //                     begin
    //                         rlSalesHeader.Init();
    //                         Evaluate(rlSalesHeader."Document Type", xlLinea.Split(cSeparador).Get(2));
    //                         rlSalesHeader.Validate("No.", xlLinea.Split(cSeparador).Get(3));
    //                         rlSalesHeader.Insert(true);
    //                         Commit();
    //                         for i := 4 to 6 do begin
    //                             if not AsignarValorCabF(xlLinea, rlSalesHeader, i) then begin

    //                                 InsertarIncidenciaF(StrSubstNo(clError, rlSalesHeader."No.", GetLastErrorText()), TempLTCNJFLogC01);

    //                             end;
    //                         end;
    //                         rlSalesHeader.Modify(true);
    //                     end;
    //                 'L':
    //                     begin
    //                         rlSalesLine.Init();
    //                         rlSalesLine.Validate("Document Type", rlSalesHeader."Document Type");
    //                         rlSalesLine.Validate("Document No.", rlSalesHeader."No.");
    //                         Evaluate(rlSalesLine."Line No.", xlLinea.Split(cSeparador).Get(2));
    //                         rlSalesLine.Insert(true);
    //                         for i := 3 to 7 do begin
    //                             if not AsignarValoresLineaF(xlLinea, rlSalesLine, i) then begin
    //                                 InsertarIncidenciaF(StrSubstNo(clError, rlSalesLine."Line No.", GetLastErrorText()), TempLTCNJFLogC01);
    //                             end;
    //                         end;
    //                         rlSalesLine.Modify(true);
    //                     end;
    //             end;
    //         end;
    //         if not TempLTCNJFLogC01.IsEmpty then begin
    //             Message('Revise incidencias importacion');
    //             Page.Run(0, TempLTCNJFLogC01);
    //         end;
    //     end else begin
    //         Error('No se ha podido subir el fichero al servidor');
    //     end;
    // end;

    // [TryFunction]
    // local procedure AsignarValorCabF(var xlLinea: Text; var rlSalesHeader: Record "Sales Header"; pCampo: Integer)
    // var
    //     xlFecha: Date;
    // begin
    //     case pCampo of
    //         4:
    //             rlSalesHeader.Validate("Sell-to Customer No.", xlLinea.Split(cSeparador).Get(4));
    //         5:
    //             rlSalesHeader.Validate("Sell-to Customer Name", xlLinea.Split(cSeparador).Get(5));
    //         6:
    //             if Evaluate(xlFecha, xlLinea.Split(cSeparador).Get(6)) then begin
    //                 rlSalesHeader.Validate("Posting Date", xlFecha);
    //             end;
    //     end;
    // end;

    // local procedure InsertarIncidenciaF(pTexto: Text)
    // var
    //     TempLTCNJFLogC01: Record TCNJFLogC01 temporary;
    // begin
    //     TempLTCNJFLogC01.Init();
    //     TempLTCNJFLogC01.Insert(true);
    //     TempLTCNJFLogC01.Texto := CopyStr(pTexto, 1, MaxStrLen(TempLTCNJFLogC01.Texto));
    //     TempLTCNJFLogC01.Modify(false);
    // end;

    // [TryFunction]

    // local procedure AsignarValoresLineaF(pLinea: Text; var prSalesLine: Record "Sales Line"; pNumCampo: Integer)
    // var
    //     xlDec: Decimal;
    // begin
    //     case pNumCampo of
    //         3:
    //             Evaluate(prSalesLine.Type, pLinea.Split(cSeparador).Get(pNumCampo));
    //         4:
    //             prSalesLine.Validate("No.", pLinea.Split(cSeparador).Get(pNumCampo));
    //         5:
    //             prSalesLine.Validate(Description, pLinea.Split(cSeparador).Get(pNumCampo));
    //         6:
    //             begin
    //                 Evaluate(xlDec)
    //             end;

    //     end;
    // end;

}