page 50110 "TCNJFDiccC01"
{
    ApplicationArea = All;
    Caption = 'Diccionario';
    PageType = List;
    SourceTable = "Integer";
    UsageCategory = Lists;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(num; Rec.Number)
                {
                    Caption = 'Nº';
                    ApplicationArea = all;
                }
                field(Campo01; ValorCeldaF(Rec.Number, 1))
                {
                    Caption = 'Valor Celda 01';
                    ApplicationArea = All;
                }
                field(Campo02; ValorCeldaF(Rec.Number, 2))
                {
                    Caption = 'Valor Celda 02';
                    ApplicationArea = All;
                }
                field(Campo03; ValorCeldaF(Rec.Number, 3))
                {
                    Caption = 'Valor Celda 03';
                    ApplicationArea = All;
                }
                field(Campo04; ValorCeldaF(Rec.Number, 4))
                {
                    Caption = 'Valor Celda 04';
                    ApplicationArea = All;
                }
                field(Campo05; ValorCeldaF(Rec.Number, 5))
                {
                    Caption = 'Valor Celda 05';
                    ApplicationArea = All;
                }
                field(Campo06; ValorCeldaF(Rec.Number, 6))
                {
                    Caption = 'Valor Celda 06';
                    ApplicationArea = All;
                }
                field(Campo07; ValorCeldaF(Rec.Number, 7))
                {
                    Caption = 'Valor Celda 07';
                    ApplicationArea = All;
                }
                field(Campo08; ValorCeldaF(Rec.Number, 8))
                {
                    Caption = 'Valor Celda 08';
                    ApplicationArea = All;
                }
                field(Campo09; ValorCeldaF(Rec.Number, 9))
                {
                    Caption = 'Valor Celda 09';
                    ApplicationArea = All;
                }
                field(Campo10; ValorCeldaF(Rec.Number, 10))
                {
                    Caption = 'Valor Celda 10';
                    ApplicationArea = All;
                }

            }
        }
    }
    var
        xDicc: Dictionary of [Integer, List of [Decimal]];



    local procedure ValorCeldaF(pFila: Integer; pColumna: Integer): Decimal
    begin
        exit(xDicc.Get(pFila).Get(pColumna));
    end;

    trigger OnOpenPage()
    var
        xlFila: Integer;
        xlClomuna: Integer;
        xlNumeros: List of [Decimal];
    begin
        for xlFila := 1 to 10 do begin
            for xlClomuna := 1 to 10 do begin
                xlNumeros.Add(Random(55555));
            end;
            xDicc.Add(xlFila, xlNumeros);
        end;
        Rec.SetRange(Number, 1, 10);
        EjemploNotificacionF()
    end;

    // trigger OnOpenPage()
    // var
    //     xlColumna: Integer;
    //     xlFila: Integer;
    //     xlNumeros: List of [Decimal];
    // begin
    //     for xlFila := 1 to 10 do begin
    //         Clear(xlNumeros);
    //         for xlColumna := 1 to 10 do begin
    //             xlNumeros.Add(Random(55555));
    //         end;
    //         xDicc.Add(xlFila, xlNumeros);
    //     end;
    //     Rec.SetRange(Number, 1, 10);
    // EjemploNotificacionF()
    // end;

    local procedure EjemploNotificacionF()
    var
        xlNotificacion: Notification;
        clmensaje: Label 'Carga finalizada';
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Id(CreateGuid());
            xlNotificacion.Message(StrSubstNo(clmensaje, i));
            xlNotificacion.Send();
        end;

        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::TCNJFFuncionesAPPC01, 'AbrirListaClientesF');
        xlNotificacion.SetData('CodigoCliente', '1000');
        xlNotificacion.Send();
    end;
}
