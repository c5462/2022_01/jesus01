page 50104 "TCNJFFichaCabVacunacionC01"
{
    Caption = 'Cab. Plan Vacunación';
    PageType = Document;
    SourceTable = TCNJFCabPlanVacunacionC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlanificada; Rec.FechaInicioVacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = '';
                    ApplicationArea = all;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    ApplicationArea = All;
                    Caption = 'Nombre empresa vacunadora';
                    ToolTip = 'aparecera nombre proveedor ';
                }


            }

            part(Lineas; TCNJFSubLinPlanVacunaC01)
            {
                ApplicationArea = All;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
            }
        }

    }
    actions
    {
        area(Processing)
        {
            action(CrearLineas)
            {

                ApplicationArea = all;
                Caption = 'Crear Lineas bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 1000 do begin
                        //KEYS CodigoLineas, NoLinea
                        rlTCNJFLinPlanVacunacionC01.Init();
                        rlTCNJFLinPlanVacunacionC01.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlTCNJFLinPlanVacunacionC01.Validate(NoLinea, i * 10);
                        rlTCNJFLinPlanVacunacionC01.Insert(true);
                    end;
                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
            action(CrearLineasNoBulk)
            {

                ApplicationArea = all;
                Caption = 'Crear Lineas no bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 1000 do begin
                        //KEYS CodigoLineas, NoLinea
                        rlTCNJFLinPlanVacunacionC01.Init();
                        rlTCNJFLinPlanVacunacionC01.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlTCNJFLinPlanVacunacionC01.Validate(NoLinea, i * 10);
                        if not rlTCNJFLinPlanVacunacionC01.Insert(true) then begin
                            error('No se pudo insertar la linea %1 del plan %2', rlTCNJFLinPlanVacunacionC01.NoLinea, rlTCNJFLinPlanVacunacionC01.CodigoLineas)
                        end;
                    end;
                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
            action(ModLineas)
            {

                ApplicationArea = all;
                Caption = 'Modificar lineas';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlTCNJFLinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
                    rlTCNJFLinPlanVacunacionC01.ModifyAll(FechaVacunacion, Today, true);
                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
            action(ModLinNoBulk)
            {

                ApplicationArea = all;
                Caption = 'Modificar lineas no bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlTCNJFLinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);

                    if rlTCNJFLinPlanVacunacionC01.FindSet(true, false) then begin
                        repeat
                            rlTCNJFLinPlanVacunacionC01.Validate(FechaVacunacion, Today + 1);
                            rlTCNJFLinPlanVacunacionC01.Modify(true);
                        until rlTCNJFLinPlanVacunacionC01.Next() = 0;

                    end;

                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
        }
    }
}
