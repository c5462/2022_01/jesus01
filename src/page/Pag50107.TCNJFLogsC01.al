page 50107 "TCNJFLogsC01"
{
    ApplicationArea = All;
    Caption = 'Logs';
    PageType = List;
    SourceTable = TCNJFLogC01;
    UsageCategory = Lists;
    DeleteAllowed = true;
    InsertAllowed = false;
    ModifyAllowed = false;
    SourceTableView = sorting(SystemCreatedAt) order(descending);

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IdUnico; Rec.IdUnico)
                {
                    ToolTip = 'Specifies the value of the IdUnico field.';
                    ApplicationArea = All;
                    Visible = false;

                }
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
