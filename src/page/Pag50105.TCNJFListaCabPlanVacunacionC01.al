page 50105 "TCNJFListaCabPlanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Cab. Plan Vacunacion C01';
    PageType = List;
    SourceTable = TCNJFCabPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = TCNJFFichaCabVacunacionC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlanificada; Rec.FechaInicioVacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(EjemploVariables01)
            {
                Caption = 'Ejemplo de variables 01';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culTCNJFFuncionesAPPC01: Codeunit TCNJFFuncionesAPPC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Al dominos que hay';
                    culTCNJFFuncionesAPPC01.EjemploVariableESF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariables02)
            {
                Caption = 'Ejemplo de variables objetos';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culTCNJFFuncionesAPPC01: Codeunit TCNJFFuncionesAPPC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Al dominos que hay que ir ');
                    culTCNJFFuncionesAPPC01.EjemploVariableE02SF(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(Bingo)
            {
                Caption = 'Bingo';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culTCNJFFuncionesAPPC01: Codeunit TCNJFFuncionesAPPC01;
                begin
                    culTCNJFFuncionesAPPC01.EjemploArrayF();
                end;
            }
            action(PruebaFncSegPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba segundo plano';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    //Codeunit.Run(Codeunit::TCNJFCodeUnitLentaC01);
                    StartSession(xlSesionIniciada, Codeunit::TCNJFCodeUnitLentaC01);
                end;
            }
            action(PruebaFncPriPlano)
            {
                ApplicationArea = all;
                Caption = 'prueba primer plano';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    //Codeunit.Run(Codeunit::TCNJFCodeUnitLentaC01);
                    StartSession(xlSesionIniciada, Codeunit::TCNJFCodeUnitLentaC01);
                end;
            }
            action(Logs)
            {
                ApplicationArea = All;
                Caption = 'Logs';
                Image = Log;
                RunObject = page TCNJFLogsC01;
            }
            action(CrearLineas)
            {

                ApplicationArea = all;
                Caption = 'Crear Lineas bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 1000 do begin
                        //KEYS CodigoLineas, NoLinea
                        rlTCNJFLinPlanVacunacionC01.Init();
                        rlTCNJFLinPlanVacunacionC01.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlTCNJFLinPlanVacunacionC01.Validate(NoLinea, i * 10);
                        rlTCNJFLinPlanVacunacionC01.Insert(true);
                    end;
                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
            action(CrearLineasNoBulk)
            {

                ApplicationArea = all;
                Caption = 'Crear Lineas no bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlTCNJFLinPlanVacunacionC01: Record TCNJFLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 1000 do begin
                        //KEYS CodigoLineas, NoLinea
                        rlTCNJFLinPlanVacunacionC01.Init();
                        rlTCNJFLinPlanVacunacionC01.Validate(CodigoLineas, rec.CodigoCabecera);
                        rlTCNJFLinPlanVacunacionC01.Validate(NoLinea, i * 10);
                        if not rlTCNJFLinPlanVacunacionC01.Insert(true) then begin
                            error('No se pudo insertar la linea %1 del plan %2', rlTCNJFLinPlanVacunacionC01.NoLinea, rlTCNJFLinPlanVacunacionC01.CodigoLineas)
                        end;
                    end;
                    Message('Tiempo invertido %1 si', CurrentDateTime - xlInicio);
                end;
            }
        }
    }

    procedure GetSelectionFilterF(var prTCNJFCabPlanVacunacionC01: Record TCNJFCabPlanVacunacionC01)

    begin
        CurrPage.SetSelectionFilter(prTCNJFCabPlanVacunacionC01);

    end;
}
