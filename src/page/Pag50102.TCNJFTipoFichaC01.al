page 50102 "TCNJFTipoFichaC01"
{
    Caption = 'TipoFichaC01';
    PageType = Card;
    SourceTable = TCNJFVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usara para editar registros de una talba donde tendremos tipos varios de registros';


    layout
    {
        area(content)
        {
            group(General)
            {
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Inidicara si la el registro es de vacunas............................y otros';
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(Fecha2AVac; Rec.Fecha2AVac)
                {

                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                group(Informacion)
                {
                    Editable = false;
                    field(SystemCreatedAt; Rec.SystemCreatedAt)
                    {
                        ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                        ApplicationArea = All;
                    }
                    field(SystemCreatedBy; Rec.SystemCreatedBy)
                    {
                        ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                        ApplicationArea = All;

                        Visible = false;
                    }
                    field(SystemModifiedAt; Rec.SystemModifiedAt)
                    {
                        ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                        ApplicationArea = All;
                    }
                    field(SystemModifiedBy; Rec.SystemModifiedBy)
                    {
                        ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                        ApplicationArea = All;
                        Visible = false;
                    }
                }

            }
        }
    }
}