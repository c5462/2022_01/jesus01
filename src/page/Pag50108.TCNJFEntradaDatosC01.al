page 50108 "TCNJFEntradaDatosC01"
{
    Caption = 'Entrada de Datos Varios';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            field(Texto01; mTextos[1])
            {
                Caption = 'xTexto01';
                ApplicationArea = all;
                Visible = xTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::texto, 1];
            }
            field(Texto02; mTextos[2])
            {
                Caption = 'xTexto02';
                ApplicationArea = all;
                Visible = xTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::texto, 2];
            }
            field(Texto03; mTextos[3])
            {
                Caption = 'xTexto03';
                ApplicationArea = all;
                Visible = xTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::texto, 3];
            }
            field(Texto04; mTextos[4])
            {
                Caption = 'xTexto04';
                ApplicationArea = all;
                Visible = xTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::texto, 4];
            }
            field(Bool01; mbooleanArray[1])
            {
                ApplicationArea = all;
                Visible = xboolVisible1;
                Caption = 'bool01';
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; mbooleanArray[2])
            {
                ApplicationArea = all;
                Visible = xboolVisible2;
                Caption = 'bool02';
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; mbooleanArray[3])
            {
                ApplicationArea = all;
                Visible = xboolVisible3;
                Caption = 'bool03';
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }
            field(Dec01; mDecimalArray[1])
            {
                ApplicationArea = all;
                Visible = xDecimVisible1;
                Caption = 'decimal 1';
                CaptionClass = mCaptionClass[xTipoDato::numerico, 1];
            }
            field(Dec02; mDecimalArray[2])
            {
                ApplicationArea = all;
                Visible = xDecimVisible2;
                Caption = 'decimal 2';
                CaptionClass = mCaptionClass[xTipoDato::numerico, 2];
            }
            field(Dec03; mDecimalArray[3])
            {
                ApplicationArea = all;
                Visible = xDecimVisible3;
                Caption = 'decimal 3';
                CaptionClass = mCaptionClass[xTipoDato::numerico, 3];
            }
            field(Date01; mDateArray[1])
            {
                ApplicationArea = all;
                Visible = xDateVisible1;
                Caption = 'date 1';
                CaptionClass = mCaptionClass[xTipoDato::hora, 1];
            }
            field(Date02; mDateArray[2])
            {
                ApplicationArea = all;
                Visible = xDateVisible2;
                Caption = 'date 2';
                CaptionClass = mCaptionClass[xTipoDato::hora, 2];
            }
            field(Date03; mDateArray[3])
            {
                ApplicationArea = all;
                Visible = xDateVisible3;
                Caption = 'date 3';
                CaptionClass = mCaptionClass[xTipoDato::hora, 3];
            }
            field(Pass01; mPassArray[1])
            {
                ApplicationArea = all;
                Visible = xPassVisible1;
                Caption = 'pass 1';
                CaptionClass = mCaptionClass[xTipoDato::password, 1];
                ExtendedDatatype = Masked;
            }
            field(Pass02; mPassArray[2])
            {
                ApplicationArea = all;
                Visible = xPassVisible2;
                Caption = 'pass 2';
                CaptionClass = mCaptionClass[xTipoDato::password, 2];
                ExtendedDatatype = Masked;
            }
            field(Pass03; mPassArray[3])
            {
                ApplicationArea = all;
                Visible = xPassVisible3;
                Caption = 'pass 3';
                CaptionClass = mCaptionClass[xTipoDato::password, 3];
                ExtendedDatatype = Masked;

            }
        }
    }
    var
        xTipoDato: option null,texto,Booleano,numerico,hora,password;
        xTipoPuntero: option null,Pasado,Leido;
        mDecimalArray: array[5] of Decimal;
        mDateArray: array[5] of Date;

        mbooleanArray: array[5] of Boolean;
        mCaptionClass: array[10, 10] of Text;
        mTextos: array[5] of Text;

        mPassArray: array[5] of Text;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        xTxtVisible04: Boolean;
        xDecimVisible1: Boolean;
        xDecimVisible2: Boolean;
        xDecimVisible3: Boolean;
        xboolVisible1: Boolean;
        xboolVisible2: Boolean;
        xboolVisible3: Boolean;
        xDateVisible1: Boolean;
        xDateVisible2: Boolean;
        xDateVisible3: Boolean;
        xPassVisible1: Boolean;
        xPassVisible2: Boolean;
        xPassVisible3: Boolean;
        mPunteros: array[2, 5] of Integer;

    /// <summary>
    /// Hace que se muestre un campo tipo texto con el caption y valor pasado 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::texto] += 1;
        mCaptionClass[xTipoDato::texto, mPunteros[xTipoPuntero::pasado, xTipoDato::texto]] := pCaption;
        mTextos[mPunteros[xTipoPuntero::pasado, xTipoDato::texto]] := pValorInicial;
        xTxtVisible01 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 1;
        xTxtVisible02 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 2;
        xTxtVisible03 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 3;
        xTxtVisible04 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 4;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo booleano con el caption y valor pasado 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano]] := pCaption;
        mbooleanArray[mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano]] := pValorInicial;

        xboolVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 1;
        xboolVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 2;
        xboolVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo decimal con el caption y valor pasado 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] += 1;
        mCaptionClass[xTipoDato::numerico, mPunteros[xTipoPuntero::pasado, xTipoDato::numerico]] := pCaption;
        mDecimalArray[mPunteros[xTipoPuntero::pasado, xTipoDato::numerico]] := pValorInicial;
        xDecimVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 1;
        xDecimVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 2;
        xDecimVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 3;
    end;

    /// <summary>
    /// Hace que se muestre un campo tipo fecha con el caption y valor pasado 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: date)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::hora] += 1;
        mCaptionClass[xTipoDato::hora, mPunteros[xTipoPuntero::pasado, xTipoDato::hora]] := pCaption;
        mDateArray[mPunteros[xTipoPuntero::pasado, xTipoDato::hora]] := pValorInicial;
        xDateVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::hora] >= 1;
        xDateVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::hora] >= 2;
        xDateVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::hora] >= 3;
    end;


    /// <summary>
    /// Hace que se muestre un campo tipo texto y una booleana con el caption y valor pasado 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: text; pPassword: Boolean)
    var
    begin
        if pPassword then begin
            mPunteros[xTipoPuntero::pasado, xTipoDato::password] += 1;
            mCaptionClass[xTipoDato::password, mPunteros[xTipoPuntero::pasado, xTipoDato::password]] := pCaption;
            mPassArray[mPunteros[xTipoPuntero::pasado, xTipoDato::password]] := pValorInicial;
            xPassVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::password] >= 1;
            xPassVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::password] >= 2;
            xPassVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::password] >= 3;
        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;


    procedure CampoF(var pSalida: Boolean): Boolean;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := mbooleanArray[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Decimal): Decimal;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::numerico] += 1;
        pSalida := mDecimalArray[mPunteros[xTipoPuntero::Leido, xTipoDato::numerico]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Date): Date;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::hora] += 1;
        pSalida := mDateArray[mPunteros[xTipoPuntero::Leido, xTipoDato::hora]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Text): Text;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::password] += 1;
        pSalida := mPassArray[mPunteros[xTipoPuntero::Leido, xTipoDato::password]];
        exit(pSalida)
    end;

    procedure CampoF(): Text;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::texto] += 1;
        exit(mTextos[mPunteros[xTipoPuntero::Leido, xTipoDato::texto]]);
    end;

    procedure CampoF(pPuntero: integer): Text;
    var

    begin
        exit(mTextos[pPuntero]);
    end;

    procedure CampoF(var pSalida: Boolean; pPuntero: integer): Boolean;
    var

    begin
        pSalida := mbooleanArray[pPuntero];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Decimal; pPuntero: integer): Decimal;
    var

    begin
        pSalida := mDecimalArray[pPuntero];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Date; pPuntero: integer): Date;
    var

    begin
        pSalida := mDateArray[pPuntero];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Text; pPuntero: integer): Text;
    var

    begin
        pSalida := mPassArray[pPuntero];
        exit(pSalida)
    end;

}